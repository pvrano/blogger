<?php get_header(); ?>

​
<div class="container">
	<div class="row">
		<div class="col s12 m12 l12"></div>
	</div>
	<div class="row">
		<?php 
			if (have_posts()):
				while (have_posts()) : the_post(); 
		?>
		<div class="col s6 m4 l4">
            <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <img class="activator" src="<?php if (has_post_thumbnail()) {the_post_thumbnail_url([200,100]);}else{echo get_template_directory_uri().'/assets/images/200X100-holder.png';} ?>">
                </div>
                <div class="card-content">
                    <span class="card-title activator grey-text text-darken-4"><?php the_title() ?><i class="fa fa-ellipsis-v right"></i></span>
                    <p><a href="<?php the_permalink() ?>">Read More...</a></p>
                </div>
                <div class="card-reveal">
                    <span class="card-title grey-text text-darken-4"><?php the_title() ?><i class="fa fa-close right"></i></span>
                    <p>
                        <?php the_excerpt() ?><a href="<?= the_permalink() ?>">Read More...</a>
                    </p>
                </div>
            </div>
        </div>
		<?php endwhile; endif; ?>
	</div>
</div>
​
<?php get_footer(); ?>