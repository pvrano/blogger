 <nav>
    <div class="nav-wrapper">
      <a href="#!" class="brand-logo">Logo</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <?php 
        $args = [
          'menu' => 'primary-menu',
          'menu_class' => 'right hide-on-med-and-down',
          'theme_location' => 'Primary Navigation'
        ];
        wp_nav_menu( $args );
      ?>
        <?php 
        $args = [
          'menu' => 'primary-menu',
          'menu_id' => 'mobile-demo',
          'menu_class' => 'side-nav',
          'theme_location' => 'Primary Navigation'
        ];
        wp_nav_menu( $args );
      ?>
    </div>
  </nav>