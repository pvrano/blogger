<?php

add_action( 'after_setup_theme', 'register_primary_menu' );
function register_primary_menu() {
  register_nav_menu( 'primary', __( 'Primary Menu', 'primary-menu' ) );
}

// frontpage sidebar
add_action( 'widgets_init', 'frontpage_sidebar_widgets_init' );
function frontpage_sidebar_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Frontpage Sidebar', 'frontpage-sidebar' ),
        'id' => 'frontpage-sidebar',
        'description' => __( 'Widgets in this area will be shown on the frontpage.', 'frontpage-sidebar' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget'  => '</li>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'singlepage_sidebar_widgets_init' );
function singlepage_sidebar_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Single Page Sidebar', 'singlepage-sidebar' ),
        'id' => 'singlepage-sidebar',
        'description' => __( 'Widgets in this area will be shown on the Single Page.', 'singlepage-sidebar' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
    'after_widget'  => '</li>',
    'before_title'  => '<h2 class="widgettitle">',
    'after_title'   => '</h2>',
    ) );
}
?>