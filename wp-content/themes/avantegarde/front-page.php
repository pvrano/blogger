<?php
get_header();
?>


<h3></h3>
<div class="container">
	<div class="row">
	 <br>
		 <div class="col  m8 l8 s12 bg-white right-separation-border" >
				   <section class="single-post">
		    <?php
           if ( have_posts() ) :
            while ( have_posts() ) : 
            the_post();
            ?>
            <h3><?php the_title(); ?></h3>
            <p><?PHP the_excerpt() ; ?>
           <a class="waves-effect waves-light btn" href=<?php the_permalink();?>>read more...</a>
            <hr>
           <?php endwhile; endif; ?>
          </section>
                    
		 </div>
        <div class="col m4 l4 bg-white">
        <!-- Sidebar Frontpage -->
          <?php dynamic_sidebar ('frontpage-sidebar'); ?>
        </div>
    
	</div>
</div>

<?php
get_footer();
?>


