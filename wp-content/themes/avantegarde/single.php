<?php
get_header();
?>
<div class="container">
	<div class="row">
	 <br>
		 <div class="col  m8 l8 s12 single-blog-post bg-white right-separation-border" >
		  
		 	<section class=" single-post ">
		    	<?php
            	if ( have_posts() ) :
            	while ( have_posts() ) : 
            	the_post();
            	?>
            	<div class=" blog-heading ">
            	<h1><?php the_title(); ?></h1></div>
            	<div class=" blog-content ">
            	<p><?PHP the_content(); ?></div>
            	<hr>
             <div class="row">
                
             	<p>
                  <div class="author-avtar">
                  	<ul class="row-inline">
                  		<li><?php  the_author(); ?></li>
                  		<li><?php echo get_avatar( get_the_author_meta ('ID'), 32 ); ?></li>
                  	</ul>
                  </div>
                  <div class=" author-description ">
             	 	<?php echo '<em style="font-size:85%;">';
             	  		echo the_author_description();
             	  	?>
             	  </div>
             </div>
             	<?php endwhile; endif; ?>
             </section>
        
             
		 </div>
		  <div class="col  m4 l4 s12 side-bar-posts bg-white" >
		  	<?php dynamic_sidebar ('singlepage-sidebar'); ?>
		  </div>
    </div>
</div>










<?php
get_footer();
?>